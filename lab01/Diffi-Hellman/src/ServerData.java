import java.util.HashMap;

/**
 * Klasa pomocnicza serwera - sluzy do przechowywania informacji o poszczegolnych clientach
 * 
 * @author Adam Hajduk, 209988
 *
 */

public class ServerData {

	private int mode;
	private DiffieHellman df;

	private HashMap<String, Boolean> handshake;

	public ServerData() {
		this.mode = 1;
		df = null;
		this.handshake = new HashMap<>();
		handshake.put("REQUEST_KEYS_STEP", false);
		handshake.put("SEND_A", false);
		handshake.put("REQUEST_ENCRYPTION", false);
	}

	public int getEncryptionMode() {
		return mode;
	}

	public void setEncryptionMode(int mode) {
		this.mode = mode;
	}

	public HashMap<String, Boolean> getHandshake() {
		return handshake;
	}

	/**
	 * Ustawia wartosci HashMapy zawierajacej flagi stanu polaczenia.
	 * @param key klucz wartosci do zapisania 
	 * @param flag flaga ustawiajaca dany stan polaczenia z serwerem klienta
	 */
	public void setHashMapValue(String key, boolean flag) {
		handshake.replace(key, handshake.get(key), flag);
	}

	public DiffieHellman getDf() {
		return df;
	}

	public void setDf(DiffieHellman df) {
		this.df = df;
	}

	/**
	 * Metoda informujaca czy zostalo nawiazane poprawne polaczenie z serwerem klienta. 
	 * @return zwraca true jesli wymiana kluczy byla poprawna, false przeciwnie
	 */
	public boolean isEstablishedConnection() {
		boolean keys = handshake.get("REQUEST_KEYS_STEP");
		boolean sendA = handshake.get("SEND_A");

		if (keys && sendA && df != null) {
			return true;
		} else {
			return false;
		}
	}

}
