import java.io.UnsupportedEncodingException;
import java.math.BigInteger;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Klasa do obs�ugi JSONow
 * 
 * @author Adam Hajduk, 209988
 *
 */

public class JSONParser {

	private Encryption enc;
	
	public JSONParser(){
		enc = new Encryption();
	}
	
	public String createJSONRequestKeys() throws JSONException {
		JSONObject obj = new JSONObject();
		obj.put("request", "keys");
		return obj.toString();
	}

	public String createJSONSendEncryption(int mode) throws JSONException {
		JSONObject obj = new JSONObject();
		switch(mode){
		case 1:
			obj.put("encryption", "none");
			break;
		case 2:
			obj.put("encryption", "xor");
			break;
		case 3:
			obj.put("encryption", "cezar");
			break;
		}
		return obj.toString();
	}

	public String createJSONSendMsg(String from, String message)
			throws JSONException, UnsupportedEncodingException {
		JSONObject obj = new JSONObject();
		obj.put("msg", message);
		obj.put("from", from);
		return obj.toString();
	}

	public String createJSONSendPG(String p, String g) throws JSONException {
		JSONObject obj = new JSONObject();
		obj.put("p", p);
		obj.put("g", g);
		return obj.toString();
	}

	public final String createJSONSendA(String a) throws JSONException {
		JSONObject obj = new JSONObject();
		obj.put("a", a);
		return obj.toString();
	}

	public final String createJSONSendB(String b) throws JSONException {
		JSONObject obj = new JSONObject();
		obj.put("b", b);
		return obj.toString();
	}

	public String[] parseJSONMessage(String JSON, int mode, BigInteger s) throws JSONException, UnsupportedEncodingException {
		JSONObject obj = new JSONObject(JSON);
		String[] rcvData = new String[2];
		if (obj.has("from") && obj.has("msg")) {
			rcvData[0] = obj.getString("from");
			rcvData[1] = enc.decodeBase64(obj.getString("msg"), mode, s);
		} else {
			rcvData[0] = "-1";
			rcvData[1] = "-1";
		}
		return rcvData;
	}

	public final String parseJSONRequestKeys(String JSON) throws JSONException {
		JSONObject obj = new JSONObject(JSON);
		if (obj.has("request"))
			return obj.getString("request");
		else
			return "-1";
	}

	public String parseJSONEncryption(String JSON) throws JSONException {
		JSONObject obj = new JSONObject(JSON);
		if (obj.has("encryption"))
			return obj.getString("encryption");
		else
			return "-1";
	}

	public String[] parseJSONPG(String JSON) throws JSONException {
		JSONObject obj = new JSONObject(JSON);
		String[] rcvData = new String[2];
		if (obj.has("p") && obj.has("g")) {
			rcvData[0] = obj.getString("p");
			rcvData[1] = obj.getString("g");
		} else {
			rcvData[0] = "-1";
			rcvData[1] = "-1";
		}
		return rcvData;
	}

	public String parseJSONA(String JSON) throws JSONException {
		JSONObject obj = new JSONObject(JSON);
		if (obj.has("a"))
			return obj.getString("a").toString();
		else
			return "-1";
	}

	public String parseJSONB(String JSON) throws JSONException {
		JSONObject obj = new JSONObject(JSON);
		if (obj.has("b"))
			return obj.getString("b").toString();
		else
			return "-1";
	}
}
