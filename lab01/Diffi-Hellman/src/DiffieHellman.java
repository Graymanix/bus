import java.math.BigInteger;
import java.security.SecureRandom;

/**
 * Klasa z metodami do kluczowych element�w protoko�u
 * 
 * @author Adam Hajduk, 209988
 *
 */
public class DiffieHellman {

	private BigInteger numP;
	private BigInteger numG;
	private BigInteger numX;
	private BigInteger numXs;
	private BigInteger key;

	private String[] numsP = {
			"81825b14201b988ffb29476736bc794a8082f8526b0d9f51f9c34c4cc0213213bb27702885a4e2b99ec43ae9b9b15bd458cf1b553622aea13c351f560c703db197666d4f7f17c9ee6d34b228e3e96285e33327a20a1477be544787d0ac00a8aa000634e4773627a5e64dfafa3c8ef666837fdcb108184ef1eb521ad944ebccf2d57b17e0c5b212dd7cf8bac6b5feb52ae11c276f335b78ae313723b8401115f9e312f10b662b05452f84fee5a88545cfcda5446599f73bdd0a96a71f698f684febf6ec229a4724a5f2483d32c24258d8701a1637edca500833afbc5c399e8c14c231993945e4448ae541659b65a3afd388e1b90a870dbf84494f1b33b779c4d3",
			"eb976259882eb0d1401346f61a19450a9ad29233c001b8d6c07560250cdc7c344ded4f954da8d8cc9b8854839437acad5dc37fd0727c1121309b9091c5e412d8553d514df1e6070fb2a1c792ce9439e024b33756cb739513d6153447bdbbc6eb5454ca32820f1596197008b60552db9d70b96a05bbcb726c9f31a6d5a542dfb3996d26b7f38e0ba014ede94397cff0fdee3b8d60bbe7ee94064ee3c02d4dcdd4da4b3f49326e2d9cb85db1edef2ac26e9b09a44be18f5038a4abe501151b4408b55640be24bd8d977606b32e81c3296915a355e6649277ca93251a8d4a21d53b659855416a2517733e04e765aee3d8c23f43ef456306a651709ab4b87073c8fb",
			"f13c82c02adce773d604f0ca10077bc508d56b8bf7b21f4014435ca7e27e548c8c90b64cc1b0327604c87921b7f390bd9ad74a6b419b529ce028ff21b9a466b3baa6d2ac40d0955a34857c8bd4cb2879158b6b0dcab2ffea3cfda06c9a6b62ad4abf4b5eb07d6c909bcba46babe76fb7e20f8049bd7e63aac0f203a57f431319712c27c904806f5331e0603b779954619668206e382d7cc5b0d36365c1d3092f5df604c235823b7be9835ad3fc1e0c3e16e0a9195040f51c33381863bde329d77774551e2d3b2ddb31ecf7f270a28de47b5e48f73e65382f265b81ecbb5ccd05e9014e09daa822af637986fd1406460cf71a4c1c4ce60ef938da6273e7b4d023",
			"a88287ef56bd5dbbfad69b983605f08fb444235141832a4bdcfb883b9319eb28ec5cf90bc0a8a3b4d7061129536d796b45ff357358ead8052d91824307dc8b2fccb582540f812cab9428975eb7b3ff5027fd66ad197d8a325a4e8d311dacf196ddc4cfebe5a8e1184f98e44d00d1d99057494565a45fb27dbfd33d0375c254504ad0ffebf6b06dd9edefdc2f9b942ffd095adade736fc1205f5272e9b323a64653b88609f310a7dcb74b31e789e7e5479678a0dee4371090037435a7e3e0be9e4d95425a4fcc15321968f84f78647eae25b12beb2a7f56afbd2da78d77a06e882d0b650a57bcb1a8a8366722816197202aa67b49f556e103beefbc32a69f0a9b",
			"e0564fc4403edb634f511ec3f8040d40d4ae9a04ada5e1eddf560f2b9aa54fa61cb32169efbd283be2990a880bb1033650768871b4e76b5272f706177c817da833a614dd2d60bf52aa70e33356c437857f9699547c22674bceb54de10cf53940cf3f26ae9366898e6f8a23241dc9ca013bd43831ca41e1a845921d39ff8d823dfe3973be96a70b415ab0044eb8ab697b17d9f3f215d86e1b2991f0add23f4b7b16d31b36af0f55bdb5929558a8fcf97aa5259845c555836dbfc3298902e35ee85a11bdb348d474a702ad03429159677fac13b3cd5c3f89d03dba00208584c025977dbc0277d69f05c970276b68ba334afa2b4118e6fcb281e4175b1acf9f93fb",
			"ff7e30f3af7f35b1dd7046590cd662245edd7b9f06870103f6a980018e0a3af85fe7cf094fe4026a2826c4de7c0f92811f8696ab7c5decfc127af00324fa2abd02a2b70a394c7d74890a77384634def1e772935cfcfc24320b38fe76e53b600d2e02661256a863b6636a4577198bfda4d7bc6c6b8609dffae08a6ccaab9e9ae5d51c68988eb9ae0ff0e84ec8daa3ba00d9cae3bc9e28eb040a0b5463bcba17b4948f7cea398267b6ae95b9832589b53a73ba3da43f3f34825ce31babb1481bc5a1cbeb94d77fd22c61f3fdd044f9a99bf3036c71613ee78c589ee863d272ead6070ab1567bcafd1b6fce5410e864d71caae0388607fcaea14ac01317dada0a1b"};

	int p = 23;
	int g = 5;
	int x = 15;

	public DiffieHellman(boolean smallIntntValues, BigInteger numP, BigInteger numG) {

		int bitLength = 2050;
		SecureRandom rnd = new SecureRandom();
		numX = BigInteger.probablePrime(bitLength, rnd);
		
		// jezeli P i G od serwera s� null wylosuj warto��
		if (numP == null && numG == null) {
			if (!smallIntntValues) {
				randBigIntegersNumber();
			} else {
				numP = BigInteger.valueOf(p);
				numG = BigInteger.valueOf(g);
			}
		} else {
			this.numG = numG;
			this.numP = numP;
		}
	}

	/**
	 * Metoda do wylosowania duzych liczb pierwszych.
	 */
	private void randBigIntegersNumber() {
		SecureRandom rnd = new SecureRandom();
		int index = rnd.nextInt(6);
		numP = new BigInteger(numsP[index], 16);
		numG = BigInteger.valueOf(2);
	}

	/**
	 * Motoda do obliczenia klucza
	 * 
	 * @param numXs
	 *            odebrana wartosc (A/B)
	 */
	public void calculateKey(BigInteger numXs) {
		key = numXs.modPow(numX, numP);
	}

	/**
	 * Zwraca obliczony klucz tajny.
	 * 
	 * @return
	 */
	public BigInteger getKey() {
		return key;
	}

	/**
	 * Metoda slu��ca do wyznaczenia klucza do wys�ania
	 */
	public void calculateAndStoreSendKey() {
		numXs = numG.modPow(numX, numP);
	}
	
	/**
	 * @return wartosc P
	 */
	public BigInteger getNumP() {
		return numP;
	}

	/**
	 * @return zwraca wartosc G
	 */
	public BigInteger getNumG() {
		return numG;
	}

	/**
	 * @return zwraca wartosc do wyslania klucza.
	 */
	public BigInteger getNumXs() {
		return numXs;
	}

}
