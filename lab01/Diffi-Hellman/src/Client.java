import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import org.json.JSONException;

/**
 * Klasa Clienta
 * 
 * @author Adam Hajduk, 209988
 *
 */

public class Client extends JFrame{

	private static final long serialVersionUID = 1L;
	
	public String name;
	public Encryption enc;
	public JTextArea textArea;
	public JTextField textField_1;
	public AtomicBoolean flag;
	public DiffieHellman df;
	public ArrayList<String> messages;
	public SocketChannel clientChannel;
	public JSONParser parser;

	private ReadWriteThread workerRead;
	private ReadWriteThread workerWrite;
	private String textToSend;
	private int mode = 1;
	private ByteBuffer buffer;
	private InetSocketAddress hostAddress;
	private static int BUFFER_SIZE = 4096;
	
	public Client(String name, String encryption) throws IOException, JSONException {
		
		setTitle("Client Application");
		setSize(509, 380);
		getContentPane().setLayout(null);

		textArea = new JTextArea();
		textArea.setLineWrap(true);
		textArea.setBounds(10, 16, 464, 219);

		textArea.setEditable(false);
		textArea.setColumns(10);

		JPanel panel = new JPanel();
		panel.setLocation(0, 0);
		panel.setSize(487, 302);
		panel.setLayout(null);
		getContentPane().add(panel);

		JLabel lblWiadomo = new JLabel("Message:");
		lblWiadomo.setBounds(10, 234, 81, 27);
		panel.add(lblWiadomo);

		textField_1 = new JTextField();
		textField_1.setBounds(10, 261, 462, 36);
		panel.add(textField_1);
		textField_1.setColumns(10);

		JScrollPane scrollPane = new JScrollPane(textArea);
		scrollPane.setBounds(10, 32, 462, 205);
		panel.add(scrollPane);
		
		JLabel lblChatLog = new JLabel("Chat log:");
		lblChatLog.setBounds(12, 13, 56, 16);
		panel.add(lblChatLog);
		
				JButton btnWylij = new JButton("Wy�lij");
				btnWylij.setBounds(10, 301, 469, 23);
				getContentPane().add(btnWylij);
				btnWylij.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						String text = textField_1.getText();
						textField_1.setText("");
						setText(text);
					}
				});

		setDefaultCloseOperation(EXIT_ON_CLOSE);
		
		this.enc = new Encryption();
		this.name = name;
		this.parser = new JSONParser();
		
		switch(encryption){
		case "none":
			mode = 1;
			break;
		case "xor":
			mode = 2;
			break;
		case "cezar":
			mode = 3;
			break;
		}
		
		flag = new AtomicBoolean(true);
		hostAddress = new InetSocketAddress("localhost", 8090);
		clientChannel = SocketChannel.open(hostAddress);
		buffer = ByteBuffer.allocate(BUFFER_SIZE);
		messages = new ArrayList<>();
		textToSend = "";
		int retVal = connectToServer();
		if (retVal == 0) {
			try {
				workerRead = new ReadWriteThread(false, this, mode);
				workerWrite = new ReadWriteThread(true, this, mode);
			} catch (IOException e) {
				e.printStackTrace();
			}
			Thread threadOne = new Thread(workerRead);
			Thread threadTwo = new Thread(workerWrite);

			setVisible(true);
			threadOne.start();
			threadTwo.start();
			try {
				threadOne.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}else {
			JOptionPane.showMessageDialog(this,"Po��czenie zosta�o zamkniete - serwer co� kombinuje :P");
			System.exit(1);
		}
	}
	
	private int connectToServer() throws IOException, JSONException {
		String receivedData = "";
		String text = "";
		buffer = ByteBuffer.wrap(new String(parser.createJSONRequestKeys()).getBytes());
		clientChannel.write(buffer);
		buffer = ByteBuffer.allocate(32768);
		text = readAllDataFromInput();
		String[] tmp = parser.parseJSONPG(new String(text));
		buffer.clear();
		if (tmp[0].equals("-1")) {
			clientChannel.close();
			return -1;
		} else if (!isTextHasOnlyDigits(tmp[0]) || !isTextHasOnlyDigits(tmp[1])) {
			clientChannel.close();
			return -1;
		}

		df = new DiffieHellman(false, new BigInteger(tmp[0]), new BigInteger(tmp[1]));
		df.calculateAndStoreSendKey();

		buffer = ByteBuffer.wrap(new String(parser.createJSONSendA(df.getNumXs().toString())).getBytes());
		clientChannel.write(buffer);
		buffer.clear();

		text = readAllDataFromInput();
		receivedData = parser.parseJSONB(new String(text));
		
		if (receivedData.equals("-1")) {
			clientChannel.close();
			return -1;
		}else if(!isTextHasOnlyDigits(receivedData)) {
			clientChannel.close();
			return -1;
		}
		buffer.clear();
		df.calculateKey(new BigInteger(receivedData));
		buffer = ByteBuffer.wrap(new String(parser.createJSONSendEncryption(mode)).getBytes());
		clientChannel.write(buffer);
		buffer.clear();
		return 0;
	}

	public static void main(String[] args) throws IOException {
		
		try {
			new Client(args[0], args[1]);
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}

	}
	
	public String readAllDataFromInput() throws IOException {
		StringBuffer bf = new StringBuffer();
		ByteBuffer buffer = ByteBuffer.allocate(BUFFER_SIZE);
		int numRead = -1;
		byte[] data;

		while ((numRead = clientChannel.read(buffer)) > 0) {
			data = new byte[numRead];
			System.arraycopy(buffer.array(), 0, data, 0, numRead);
			bf.append(new String(data));
			buffer.clear();
			if (numRead <= 4096)
				break;
		}
		if (numRead == -1) {
			clientChannel.close();
		}

		return bf.toString();
	}

	public String getText() throws InterruptedException {
		synchronized (workerWrite) {
			workerWrite.wait();
			return textToSend;
		}
	}

	private void setText(String text) {
		synchronized (workerWrite) {
			textToSend = text;
			workerWrite.notify();
		}
	}

	private boolean isTextHasOnlyDigits(String text) {
		String pattern = "^[1-9]([0-9]*$)";
		return text.matches(pattern);
	}
}
