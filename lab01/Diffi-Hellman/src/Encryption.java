import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.util.Base64;

/**
 * Klasa do obs�ugi szyfrowania
 * 
 * @author Adam Hajduk, 209988
 *
 */

public class Encryption {

	/**
	 * metoda do szyfrowania wiadomosci
	 * 
	 * @param msg
	 *            wiadomosc
	 * @param mode
	 *            tryb kodowania (1/2/3)
	 * @param s
	 *            klucz
	 * @return zaszyfrowana wiadomosc
	 * @throws UnsupportedEncodingException
	 */
	public String encryptBase64(String msg, int mode, BigInteger s) throws UnsupportedEncodingException {
		return Base64.getEncoder().encodeToString(encrypt(msg, mode, s).getBytes());
	}

	/**
	 * Metoda do odszyfrowania wiadomosci
	 * 
	 * @param msg
	 *            wiadomosc
	 * @param mode
	 *            tryb kodowania (1/2/3)
	 * @param s
	 *            klucz
	 * @return odszyfrowana wiadomosc
	 * @throws UnsupportedEncodingException
	 */

	public String decodeBase64(String msg, int mode, BigInteger s) throws UnsupportedEncodingException {
		return decrypt(new String(Base64.getDecoder().decode(msg.getBytes())), mode, s);
	}

	/**
	 * metoda wybierajaca szyfrowanie (none/cezar/cor)
	 * 
	 * @param msg
	 *            wiadomosc
	 * @param mode
	 *            tryb kodowania (1/2/3)
	 * @param s
	 *            klucz
	 * @return zaszyfrowana wiadomosc
	 * @return
	 */
	private String encrypt(String msg, int mode, BigInteger s) {

		switch (mode) {
		case 1:
			return msg;
		case 2:
			try {
				return encryptDecryptXOR(msg, s);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		case 3:
			try {
				return encryptCezar(msg, s);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		default:
			return msg;
		}
	}

	/**
	 * Metoda do odszyfrowania wiadomosci
	 * 
	 * @param msg
	 *            wiadomosc
	 * @param mode
	 *            tryb kodowania (1/2/3)
	 * @param s
	 *            klucz
	 * @return odszyfrowana wiadomosc
	 * @throws UnsupportedEncodingException
	 */

	private String decrypt(String msg, int mode, BigInteger s) {

		switch (mode) {
		case 1:
			return msg;
		case 2:
			try {
				return encryptDecryptXOR(msg, s);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		case 3:
			try {
				return decryptCezar(msg, s);
			} catch (UnsupportedEncodingException e) {
				e.printStackTrace();
			}
		default:
			return msg;
		}
	}

	/**
	 * Metoda szyfrujaca wiadomosc szyfrem Cezara
	 * 
	 * @param msg
	 *            wiadomosc
	 * @param s
	 *            klucz
	 * @return zaszyfrowana wiadomosc
	 * @return
	 */

	private String encryptCezar(String msg, BigInteger s) throws UnsupportedEncodingException {

		BigInteger mod = BigInteger.valueOf(128);
		s = s.mod(mod);
		int s2 = s.intValue();
		byte[] msgBytes = msg.getBytes("utf-8");

		for (int i = 0; i < msgBytes.length; i++) {
			if ((int) msgBytes[i] + s2 > 127) {
				msgBytes[i] = (byte) ((int) msgBytes[i] + s2 - 127);
			} else {
				msgBytes[i] += (byte) s2;
			}

		}

		return new String(msgBytes);
	}

	/**
	 * Metoda odszyfrujaca wiadomosc szyfrem Cezara
	 * 
	 * @param msg
	 *            wiadomosc
	 * @param s
	 *            klucz
	 * @return odszyfrowana wiadomosc
	 * @return
	 */

	private String decryptCezar(String msg, BigInteger s) throws UnsupportedEncodingException {

		BigInteger mod = BigInteger.valueOf(128);
		s = s.mod(mod);
		int s2 = s.intValue();
		byte[] msgBytes = msg.getBytes("utf-8");
		for (int i = 0; i < msgBytes.length; i++) {
			if ((int) msgBytes[i] - s2 < 0) {
				msgBytes[i] = (byte) ((int) msgBytes[i] - s2 + 127);
			} else {
				msgBytes[i] -= (byte) s2;
			}
		}

		return new String(msgBytes);
	}

	/**
	 * Metoda szyfrujaca i odszyfrowujaca wiadomosc XORem
	 * 
	 * @param msg
	 *            wiadomosc
	 * @param s
	 *            klucz
	 * @return zaszyfrowana/odszyfrowana wiadomosc
	 * @return
	 */
	private String encryptDecryptXOR(String msg, BigInteger s) throws UnsupportedEncodingException {

		byte[] sByte = s.toByteArray();
		byte[] msgBytes = msg.getBytes();
		byte tmp = (byte) (sByte[sByte.length - 1] & 0xFF);

		for (int i = 0; i < msgBytes.length; i++) {
			msgBytes[i] = ((byte) (msgBytes[i] ^ (tmp)));
		}

		return new String(msgBytes);
	}
}
