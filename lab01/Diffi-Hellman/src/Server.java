import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigInteger;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONException;

/**
 * Klasa serwera
 * 
 * @author Adam Hajduk, 209988
 *
 */

public class Server {

	private Selector selector;
	private InetSocketAddress listenAddres;
	private ArrayList<String[]> messages;
	private JSONParser parser;

	private Encryption enc;

	public static void main(String[] args) throws IOException, JSONException {
		new Server("localhost", 8090).startServer();
	}

	public Server(String addres, int port) throws IOException {
		listenAddres = new InetSocketAddress(addres, port);
		messages = new ArrayList<>();
		this.enc = new Encryption();
		this.parser = new JSONParser();
	}

	/**
	 * Metoda sluzaca do uruchomienia serwera
	 * 
	 * @throws IOException
	 * @throws JSONException
	 */
	private void startServer() throws IOException, JSONException {
		this.selector = Selector.open();
		// pobranie i konfiguracja socketu
		ServerSocketChannel serverSocket = ServerSocketChannel.open();
		serverSocket.configureBlocking(false);
		serverSocket.socket().bind(listenAddres);
		serverSocket.register(this.selector, SelectionKey.OP_ACCEPT);
		System.out.println("Server started...");
		mainLoop();
	}

	/**
	 * Glowna petla serwera
	 * 
	 * @throws JSONException
	 */
	private void mainLoop() throws JSONException {
		Iterator<?> keys = null;
		SelectionKey key = null;
		while (true) {
			try {
				this.selector.select();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			// pobranie wszystkich podlaczonych i gotowych klientow
			keys = this.selector.selectedKeys().iterator();
			while (keys.hasNext()) {
				key = (SelectionKey) keys.next();

				if (!key.isValid()) {
					continue;
				}
				if (key.isAcceptable()) {
					try {
						accept(key);
					} catch (IOException e) {
						key.cancel();
						System.out.println("KONIEC");
					}
				} else if (key.isReadable()) {
					try {
						readMessage(key);
					} catch (IOException e) {
						key.cancel();
						System.out.println("KONIEC");
					}
				}
			}

			keys = this.selector.selectedKeys().iterator();
			while (keys.hasNext()) {
				key = (SelectionKey) keys.next();
				keys.remove();
				if (!key.isValid()) {
					continue;
				}

				if (key.isWritable()) {
					try {
						writeMessage(key);
					} catch (IOException e) {
						key.cancel();
					}
				}
			}
			messages.clear();
		}
	}

	/**
	 * Metoda sluzaca do wysylania wiadomosci
	 * 
	 * @param key
	 *            klucz klienta (ID)
	 * @throws IOException
	 */
	private void writeMessage(SelectionKey key) throws IOException {

		SocketChannel channel = (SocketChannel) key.channel();
		ByteBuffer buffer = null;
		int mode;
		String tmpMessage = "";
		String JSON = "";
		ServerData client = (ServerData) key.attachment();
		// wyslanie wszytskich wiadomosci do danego klienta
		for (String[] t : messages) {
			mode = client.getEncryptionMode();
			tmpMessage = enc.encryptBase64(t[0], mode, client.getDf().getKey());
			try {
				JSON = parser.createJSONSendMsg(t[1], tmpMessage);
				buffer = ByteBuffer.wrap(JSON.getBytes());
				channel.write(buffer);
				buffer.clear();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
	}

	/**
	 * Metoda sluaca do odbierania wiadomosci
	 * 
	 * @param key
	 *            klucz klienta(ID)
	 * @throws IOException
	 * @throws JSONException
	 */
	private void readMessage(SelectionKey key) throws IOException, JSONException {
		ServerData server = (ServerData) key.attachment();
		String text = readAllDataFromInput(key);
		String anotherData = null;
		String data;
		if (server.isEstablishedConnection() && !text.isEmpty()) {
			
			// ustawienie szyfrowania
			if (!server.getHandshake().get("REQUEST_ENCRYPTION"))
				findEncrypt(text, server);
			
			findAllMessages(text, server, server.getEncryptionMode());
		} else {
			if (!server.getHandshake().get("REQUEST_KEYS_STEP")) {
				anotherData = findPGRequest(text);
				if (anotherData != null) {
					data = parser.parseJSONRequestKeys(anotherData);
					if (isRequestKeysTextGood(data)) {
						processRequestForKeys(data, server, key);
						writeValueB(server.getDf().getNumXs(), key);
						server.setHashMapValue("REQUEST_KEYS_STEP", true);
					}
				}
			} else if (!server.getHandshake().get("SEND_A")) { 
																				
				anotherData = findSendA(text);
				boolean flag = false;
				
				if (anotherData != null) {
					
					data = parser.parseJSONA(anotherData);

					if (!data.equals("-1") && isTextHasOnlyDigits(data)) {
						flag = true;
						server.setHashMapValue("SEND_A", true);
						server.getDf().calculateKey(new BigInteger(data));
					}
				}
				
				if (flag) {
					findEncrypt(text, server);
					findAllMessages(text, server, server.getEncryptionMode());
				}
			}
		}
	}

	/**
	 * Metoda sprawdzajaca poprawnosc zapytania o klucze
	 * 
	 * @param text
	 *            zapytanie o klucze
	 * @return true jesli jest poprawne, false jesli nie.
	 */
	private boolean isRequestKeysTextGood(String text) {
		if (!text.equals("-1") && text.equals("keys")) {
			return true;
		} else
			return false;
	}

	/**
	 * Przetworzenie wiadomosci otrzymanej od klienta.
	 * 
	 * @param JSON
	 *            wiadomosc w formacie JSON
	 * @param server
	 *            klient
	 * @throws JSONException
	 * @throws UnsupportedEncodingException
	 */
	private void processMessages(String JSON, ServerData server, int mode)
			throws JSONException, UnsupportedEncodingException {
		String[] tmpData;
		String decodedData;
		tmpData = parser.parseJSONMessage(JSON, mode, server.getDf().getKey());

		if (!tmpData[0].equals("-1")) {
			decodedData = tmpData[1];
			messages.add(new String[]{decodedData, tmpData[0]});
		}
	}

	/**
	 * Metoda sluzaca do wyslanai wartosci P i G
	 * 
	 * @param valueP
	 *            wartosc P
	 * @param valueG
	 *            wartosc G
	 * @param key
	 *            klucz klienta (identyfikator)
	 * @throws IOException
	 */
	private void writeKeysPG(BigInteger valueP, BigInteger valueG, SelectionKey key) throws IOException {
		try {
			SocketChannel channel = (SocketChannel) key.channel();
			ByteBuffer buffer = null;
			String JSON = parser.createJSONSendPG(valueP.toString(), valueG.toString());

			buffer = ByteBuffer.wrap(JSON.getBytes());
			channel.write(buffer);
			buffer.clear();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Metoda odpowiedzialna za wyslanie do klienta wartosci B.
	 * 
	 * @param valueB
	 *            wartosc B
	 * @param key
	 *            klucz ID klienta
	 * @throws IOException
	 */
	private void writeValueB(BigInteger valueB, SelectionKey key) throws IOException {
		try {
			SocketChannel channel = (SocketChannel) key.channel();
			ByteBuffer buffer = null;
			String JSON = parser.createJSONSendB(valueB.toString());
			buffer = ByteBuffer.wrap(JSON.getBytes());
			channel.write(buffer);
			buffer.clear();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Metoda odpowiedzialna za odczytanie prosby o wyslanie klucza przez
	 * klienta.
	 * 
	 * @param data
	 *            prosba o wyslnie klucza
	 * @param server
	 *            klient ktory wyslal prosbe
	 * @param key
	 *            klucz identyfikujacy klienta
	 * @throws IOException
	 */
	private void processRequestForKeys(String data, ServerData server, SelectionKey key) throws IOException {
		server.setDf(new DiffieHellman(false, null, null));
		server.getDf().calculateAndStoreSendKey();
		writeKeysPG(server.getDf().getNumP(), server.getDf().getNumG(), key);
	}

	/**
	 * Metoda sluzaca do akceptowania polaczenia
	 * 
	 * @param key
	 *            klucz identyfikujacy nowego klienta
	 * @throws IOException
	 */
	private void accept(SelectionKey key) throws IOException {
		ServerSocketChannel serverChannel = (ServerSocketChannel) key.channel();

		SocketChannel channel = serverChannel.accept();
		channel.configureBlocking(false);
		channel.register(this.selector, SelectionKey.OP_READ | SelectionKey.OP_WRITE, new ServerData());
	}

	/**
	 * Metoda znajdujaca wszytskie wiadomosci w Stringu na podstawie regex.
	 * 
	 * @param text
	 *            tekst zawierajacy wiadomosci
	 * @param client
	 *            klient od ktorego zostaly wyslane wiadomosci
	 * @throws JSONException
	 * @throws UnsupportedEncodingException
	 */
	private void findAllMessages(String text, ServerData client, int mode) throws JSONException, UnsupportedEncodingException {
		Pattern pattern = Pattern.compile("(\\{\\s*\"msg\"\\s*:\\s*(.+?)\\s*,\\s*\"from\":\\s*(.+?)\\s*\\})");
		Matcher matcher = pattern.matcher(text);
		String tmpMessage = "";
		while (matcher.find()) {
			tmpMessage = matcher.group(1);
			processMessages(tmpMessage, client, mode);
		}
	}

	/**
	 * Metoda znajdujaca w wiadomosci zapytanie ekcryption.
	 * 
	 * @param text
	 *            tekst w ktorym moze znajdowac sie zapytanie o encryption
	 * @param client
	 *            klient, ktory poprosil o encyrption
	 * @throws JSONException
	 */
	private void findEncrypt(String text, ServerData client) throws JSONException {
		Pattern patternMessage = Pattern.compile("(\\{\\s*\"msg\"\\s*:\\s*(.+?)\\s*,\\s*\"from\":\\s*(.+?)\\s*\\})");
		Matcher matcherMessage = patternMessage.matcher(text);
		int messageStart = -1;
		int encryptStart = -1;

		// znalezienie wiadomosci bo moze wiadomosc wystapila wczesniej niz
		// encryption i wtedy trzeba niestety zrezygnowac z encrypt
		if (matcherMessage.find()) {
			messageStart = matcherMessage.start();
		}

		String tmp = null;
		String data = "";
		Pattern pattern = Pattern.compile("(\\{\\s*\"encryption\"\\s*:\\s*(.[^,]+?)\\s*\\})");
		Matcher matcher = pattern.matcher(text);
		// znalezienie zapytania o encrypt
		if (matcher.find()) {
			tmp = matcher.group(1);
			encryptStart = matcher.start();
		}

		// jesli wiadomosc zostala wyslana wczesniej niz encrypt to
		// automatycznie odrzucane jest kolejne encrypt i jest ustawione dla
		// klienta NONE
		if (messageStart > encryptStart || messageStart == -1) {
			if (tmp != null) {
				data = parser.parseJSONEncryption(tmp);
				// spawdzenie czy zapytanie jest poprawne
				if (isEncryptionGood(data)) {
					switch(data){
					case "none":
						client.setEncryptionMode(1);
						break;
					case "xor":
						client.setEncryptionMode(2);
						break;
					case "cezar":
						client.setEncryptionMode(3);
						break;
					}
					
					client.setHashMapValue("REQUEST_ENCRYPTION", true);
				} else if (messageStart != -1) { // sprawdzenie czy wiadomosc
													// jest w ogole znaleziona
													// jakakolwiek jak tak to
													// ustaw na NONE
					client.setHashMapValue("REQUEST_ENCRYPTION", true);
				}
			}
		} else { // jesli wiaodmosc wczensiej niz encrypt to pozostaw NONE
			client.setHashMapValue("REQUEST_ENCRYPTION", true);
		}
	}

	/**
	 * Metoda sprawdzajaca czy encrypt jest poprawnym zapytaniem.
	 * 
	 * @param encryptionText
	 *            tekst z zapytaniem
	 * @return true jesli wszytsko jest poprawne, false przeciwnie
	 */
	private boolean isEncryptionGood(String encryption) {
		if (!encryption.equals("-1")
				&& ((encryption.equals("none") || encryption.equals("cezar") || encryption.equals("xor")))) {
			return true;
		} else
			return false;
	}

	/**
	 * Metoda znajdujaca zapytanie o klucze.
	 * 
	 * @param text
	 *            zawierajacy JSONy z zapytaniem o klucze
	 * @return zwraca String z JSON zawierajacym zapytanie o klucze w formacie
	 *         JSON
	 */
	private String findPGRequest(String text) {
		Pattern pattern = Pattern.compile("(\\{\\s*\"request\"\\s*:\\s*(.[^,]+?)\\s*\\})");
		Matcher matcher = pattern.matcher(text);
		if (matcher.find()) {
			return new String(matcher.group(1));
		}
		return null;
	}

	/**
	 * Metoda znajdujaca wyslany klucz A od klienta.
	 * 
	 * @param text
	 *            tekst zawierajacyklucz A w formacie JSON
	 * @return zwraca String z JSOn z kluczem od klienta A
	 */
	private String findSendA(String text) {
		Pattern pattern = Pattern.compile("(\\{\\s*\"a\"\\s*:\\s*(.[^,]+?)\\s*})");
		Matcher matcher = pattern.matcher(text);
		if (matcher.find()) {
			return new String(matcher.group(1));
		}
		return null;
	}

	/**
	 * Metoda sprawdzajaca czy liczba zaczyna sie od 1-9 a nie od 0 lub -
	 * 
	 * @param text
	 *            liczba w formacie String
	 * @return zwraca true jesli liczba jest poprawna, false przeciwnie
	 */
	private boolean isTextHasOnlyDigits(String text) {
		String pattern = "^[1-9]([0-9]*$)";
		return text.matches(pattern);
	}

	/**
	 * Metoda odpowiedzialna za pobranie wszytskich danych od klienta.
	 * 
	 * @param key
	 *            klucz identyfikujacy klienta.
	 * @return zwraca tekst z danymi otrzymanymi od klienta.
	 * @throws IOException
	 */
	private String readAllDataFromInput(SelectionKey key) throws IOException {
		StringBuffer bf = new StringBuffer();
		SocketChannel channel = (SocketChannel) key.channel();
		ByteBuffer buffer = ByteBuffer.allocate(4096);
		int numRead = -1;
		byte[] data;
		while ((numRead = channel.read(buffer)) > 0) {
			data = new byte[numRead];
			System.arraycopy(buffer.array(), 0, data, 0, numRead);
			bf.append(new String(data));
			buffer.clear();
		}
		if (numRead == -1) {
			channel.close();
			key.cancel();
		}
		return bf.toString();
	}

}
