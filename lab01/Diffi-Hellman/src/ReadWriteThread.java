import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.SwingUtilities;

import org.json.JSONException;

/**
 * Klasa watku do odczytu/wysylania wiadomosci
 * 
 * @author Adam Hajduk, 209988
 *
 */

public class ReadWriteThread implements Runnable {

	private boolean writeThread;
	private Client c;
	private int mode;

	public ReadWriteThread(boolean flag, Client c, int mode) throws IOException {
		this.writeThread = flag;
		this.c = c;
		this.mode = mode;
	}

	private void readData() {
		try {
			String text = "";
			while (c.flag.get()) {
				text = c.readAllDataFromInput();
				if (!text.isEmpty()) {
					findAllMessages(text);
					if (c.messages.size() != 0) {

						SwingUtilities.invokeLater(new Runnable() {
							public void run() {

								String tmp = c.textArea.getText();
								StringBuffer bf = new StringBuffer();
								for (String x : c.messages) {
									bf.append(x);
								}
								c.textArea.setText(tmp + "\n" + bf.toString());
								c.revalidate();
								c.repaint();
								c.messages.clear();
							}
						});
					}
					text = "";
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private synchronized void writeData() {
		try {
			while (c.flag.get()) {
				write(c.name, c.getText());
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		if (writeThread) {
			writeData();
		} else {
			readData();
		}
		try {
			c.clientChannel.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private void write(String from, String text) throws IOException {
		ByteBuffer buffer = null;
		String tmpMessage = "";
		String JSON = "";
		tmpMessage = c.enc.encryptBase64(text, mode, c.df.getKey());
		try {
			JSON = c.parser.createJSONSendMsg(from, tmpMessage);
			buffer = ByteBuffer.wrap(JSON.getBytes());
			c.clientChannel.write(buffer);
			buffer.clear();
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}

	private void findAllMessages(String text) throws JSONException, UnsupportedEncodingException {
		Pattern pattern = Pattern.compile("(\\{\\s*\"msg\"\\s*:\\s*(.+?)\\s*,\\s*\"from\":\\s*(.+?)\\s*\\})");
		Matcher matcher = pattern.matcher(text);
		String tmpMessage = "";

		while (matcher.find()) {
			tmpMessage = matcher.group(1);
			processMessages(tmpMessage);
		}
	}

	private void processMessages(String JSON) throws JSONException, UnsupportedEncodingException {
		String[] tmpData;
		String decodedData;

		tmpData = c.parser.parseJSONMessage(JSON, mode, c.df.getKey());
		if (!tmpData[0].equals("-1")) {
				decodedData = tmpData[1];
			c.messages.add(tmpData[0] + ":" + decodedData);
		}
	}

}
