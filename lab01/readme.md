# LABORATORIUM 01 
Projekt na laboratorium zakłada wykonanie aplikacji serwer i klienta komunikujących się ze sobą przy wykorzystaniu protokołu Diffiego-Hellmana. Komunikacja odbywa się poprazez przesyłanie JSON.   
#WYMAGANIA
 - JRE (Java Runtime Environment) (1.7+)
 - (do zbudowania aplikcaji) JDK (Java Development Kit) (1.7+)
#UŻYTKOWANIE

1. Uruchomienie serwera
2. Uruchomienie klienta (1 lub więcej)

Serwer:
Serwer uruchamiamy bez dodatkowych argumentów

Klient:
Aplikacja klienta wymaga podania 2 argumentów. Pierwszy z nich to nazwa klienta, a drugi - metoda szyfrowania (none/cor/cezar)
  
## LICENCJA ver. EN
Licensed under the Apache License, Version 2.0 (the "License");    
you may not use this file except in compliance with the License.   
You may obtain a copy of the License at   

http://www.apache.org/licenses/LICENSE-2.0   

Unless required by applicable law or agreed to in writing, software   
distributed under the License is distributed on an "AS IS" BASIS,   
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.   
See the License for the specific language governing permissions and   
limitations under the License.   